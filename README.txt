Overview of automember.module
-----------------------------

This module provides automated role management for specified roles.

Applications of this module include:
1) automatically granting frequent and reliable contributers a special role, and 
2) automatically granting new users a special role.

The module was first intended for #1 as listed above.  In a small community website like my own[1], 
with active membership less than 20, some distinction was desired between the active members of 
the community and the phantom visitors who never participate.  Also, we grant some special privileges 
were to members who reliably contributed high-quality content.

After some research into Dunbar's Number and the theory of small group communication, it became 
apparent that a small community website could expect to hover around 5-9 active members. [2] But 
who are these active members, and who can be trusted to reliably provide good content?

By implementing an automated process that 
1) measures the activity of users in units of posts/week, 
2) limits the number of special users to something like 7, 
3) allows special users to invite other reliable users to a vacant seat in the special group, and 
4) removes inactive users from the special group, 
a large burden is removed from the shoulders of the site administrator.  In the implementation at 
my own website, the special group is given the ability to start new topics and post pictures.  
Other perks are also applied to the special group.

Author
------

Nic Ivy (nji@njivy.org)


README.txt version
------------------

[1] http://njivy.org
[2] http://www.lifewithalacrity.com/2004/03/the_dunbar_numb.html
